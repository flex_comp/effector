package effector

import "gitlab.com/flex_comp/util"

type Effect interface {
	Name() string
	Do(*util.ContextCE) error
}

type Mode string

const (
	SeqSoft  Mode = "seq_soft"
	SeqHard  Mode = "seq_hard"
	Parallel Mode = "parallel"
)

type EffectItem struct {
	Name string        `json:"name"`
	Args []interface{} `json:"args"`
}

type EffectConf struct {
	Mode    Mode          `json:"mode"`
	Exec    []*EffectItem `json:"exec"`
	Success []*EffectItem `json:"success"`
	Failed  []*EffectItem `json:"failed"`
}
