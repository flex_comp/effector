package effector

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/util"
	"io/ioutil"
	"os"
	"testing"
	"time"
)

type e1 struct{}

func (e *e1) Name() string {
	return "e1"
}

func (e *e1) Do(ctx *util.ContextCE) error {
	fmt.Println("e1 check begin.")
	s, _ := util.JSONMarshal(ctx)
	fmt.Println(string(s))
	fmt.Println("e1 check end.")
	return nil
}

type e2 struct{}

func (e *e2) Name() string {
	return "e2"
}

func (e *e2) Do(ctx *util.ContextCE) error {
	fmt.Println("e2 check begin.")
	s, _ := util.JSONMarshal(ctx)
	fmt.Println(string(s))
	fmt.Println("e2 check end.")
	return nil
}

type EffectorConf struct {
	Effector *EffectConf `json:"effector"`
}

func TestDo(t *testing.T) {
	_ = comp.Init(map[string]interface{}{
		"config": "./test/server.json",
	})

	Reg(new(e1))
	Reg(new(e2))

	srcFile, e := os.OpenFile("./test/effect.json", os.O_RDWR, 0666)
	if e != nil {
		t.Failed()
	}

	defer srcFile.Close()
	raw, e := ioutil.ReadAll(srcFile)
	if e != nil {
		t.Failed()
	}

	conf := new(EffectorConf)
	e = util.JSONUnmarshal(raw, conf)
	if e != nil {
		t.Failed()
	}

	go func() {
		<-time.After(time.Second)
		type args struct {
			ctx  *util.ContextCE
			conf *EffectConf
		}
		tests := []struct {
			name string
			args args
		}{
			{
				name: "all",
				args: args{
					ctx: &util.ContextCE{
						Custom:      nil,
						LocalCustom: nil,
						Config:      nil,
						External:    nil,
						Args:        nil,
					},
					conf: conf.Effector,
				},
			},
		}
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				Do(tt.args.ctx, tt.args.conf)
			})
		}
	}()

	ch := make(chan bool, 1)
	go func() {
		<-time.After(time.Second * 10)
		ch <- true
	}()

	_ = comp.Start(ch)
}
