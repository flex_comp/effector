package effector

import (
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"sync"
)

var (
	effector *Effector
)

func init() {
	effector = &Effector{
		lib: make(map[string]Effect),
	}
	_ = comp.RegComp(effector)
}

type Effector struct {
	lib map[string]Effect
}

func (e *Effector) Init(map[string]interface{}, ...interface{}) error {
	return nil
}

func (e *Effector) Start(i ...interface{}) error {
	return nil
}

func (e *Effector) UnInit() {

}

func (e *Effector) Name() string {
	return "effector"
}

func Reg(effect Effect) {
	effector.reg(effect)
}

func (e *Effector) reg(effect Effect) {
	e.lib[effect.Name()] = effect
}

func UnReg(effect Effect) {
	effector.unReg(effect)
}

func (e *Effector) unReg(effect Effect) {
	delete(e.lib, effect.Name())
}

func Do(ctx *util.ContextCE, conf *EffectConf) {
	effector.do(ctx, conf)
}

func (e *Effector) do(ctx *util.ContextCE, conf *EffectConf) {
	success := true
	switch conf.Mode {
	case SeqSoft, SeqHard:
		for _, ele := range conf.Exec {
			item, ok := e.lib[ele.Name]
			if !ok {
				log.ErrorF("执行 %v 失败, 未找到 %s 实现", conf, ele.Name)
				continue
			}

			ctx.Args = ele.Args
			err := item.Do(ctx)
			if err == nil {
				continue
			}

			success = false
			log.ErrorF("执行 %v 失败: %v", conf, err)

			if conf.Mode == SeqSoft {
				break
			}
		}
	case Parallel:
		// 并发执行
		var wg sync.WaitGroup
		for _, ele := range conf.Exec {
			item, ok := e.lib[ele.Name]
			if !ok {
				log.ErrorF("执行 %v 失败, 未找到 %s 实现", conf, ele.Name)
				continue
			}

			wg.Add(1)
			go func() {
				defer wg.Done()

				ctx.Args = ele.Args
				err := item.Do(ctx)
				if err != nil {
					success = false
					log.ErrorF("执行 %v 失败: %v", conf, err)
				}
			}()

			wg.Wait()
		}
	}

	var after []*EffectItem
	if success {
		after = conf.Success
	} else {
		after = conf.Failed
	}

	for _, ele := range after {
		item, ok := e.lib[ele.Name]
		if !ok {
			log.ErrorF("结算 %v %v 失败, 未找到 %s 实现", success, conf, ele.Name)
			continue
		}

		ctx.Args = ele.Args
		err := item.Do(ctx)
		if err != nil {
			log.ErrorF("结算 %v %v 失败: %s", success, conf, err)
		}
	}
}
